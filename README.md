## Chiapet Pipeline安装和使用说明

#### 安装

###### build docker image

```shell
#进入docker目录
cd docker
#构建docker iamge
docker build -t "chiapet:20211206" .
```

#### 使用

```shell
#建立分析目录
WORKDIR="/分析目录/绝对路径"
#创建分析目录和reads links目录
mkdir -p $WORKDIR/links
#ln -s /要分析的reads/*_[12].fastq.gz $WORKDIR/links
#reads可以用_[12].fastq.gz 和 _[12].fq.gz
ln -s /要分析的reads/*_[12].fastq.gz $WORKDIR/links
#切换到分析目录
cd $WORKDIR
#运行主程序
nextflow run ../nextflow/main.nf --reads_dir $WORKDIR/links -with-trace -bg -resume --call_peaks macs 1>o 2>e
#完成上面这一步，就可以关掉当前窗口，会在后面运行；如果要查看任务运行的状态，可以运行："cat trace.txt" 或者 "nextflow log"
```


