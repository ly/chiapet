#!/usr/bin/env nextflow
nextflow.enable.dsl = 2

def helpMessage() {

    log.info"""

    Usage:

    The typical command for running the pipeline is as follows:

		nextflow run main.nf --reads_dir testdata/ -with-trace -bg -resume --call_peaks macs2  
 
    Mandatory arguments:
        
        --reads_dir     Input the dir of all reads, which save all the reads in one dir.

	Analysis arguments:
		
		--call_peaks  [macs|macs2]. default: macs2		

   Optional Arguments:

        --outdir        Input the dir of the result output to.
        --help          Get the help information.
 
    
""".stripIndent()

}





//database = "/Xbiome1/liangyong/database/metaWRAP/db"
database = "/home/liangyong/t/database"

if (!params.reads_dir) {

    helpMessage()
    log.info"""
    [ERROR] --reads_dir is needed
    """.stripIndent()
    exit 0

}

include {

	prepare_rawdata;
	step1;
	step2_1;
	step2_2;
	step2_3;
	call_peaks_macs2;
	call_peaks_macs;
	stat;
	merged_stats;

} from './modules.nf'


workflow {

	//check outdir and build it if it does not exists
    if (!params.outdir) {
        params.outdir = "${PWD}/Result"
    }
    if(!file(params.outdir).exists()){
        file(params.outdir).mkdirs()
    }
	samples = Channel.fromFilePairs("${params.reads_dir}/*{_,.}{1,2}.f*q.gz")
	println samples.view()
	prepare_rawdata(samples, params.outdir)
	step1(prepare_rawdata.out, params.outdir)
	step2_1(step1.out, "${database}", "GRCh38.primary_assembly.genome.fa", params.outdir)
	step2_2(step1.out, "${database}", "GRCh38.primary_assembly.genome.fa", params.outdir)
	step2_3(step1.out, "${database}", "GRCh38.primary_assembly.genome.fa", params.outdir)
	
	tmp_call_peaks_out = ""
	if ( ! params.call_peaks){
		
		call_peaks_macs2(step2_1.out, step2_2.out, step2_3.out, params.outdir)	
		tmp_call_peaks_out = call_peaks_macs2.out	

	}else if (params.call_peaks == "macs2"){
		
		call_peaks_macs2(step2_1.out, step2_2.out, step2_3.out, params.outdir)
		tmp_call_peaks_out = call_peaks_macs2.out	

	}else if (params.call_peaks == "macs") {
			
		call_peaks_macs(step2_1.out, step2_2.out, step2_3.out, params.outdir)	
		tmp_call_peaks_out = call_peaks_macs.out
	}

	stat(step1.out ,step2_1.out, step2_2.out, step2_3.out, tmp_call_peaks_out, params.outdir)	
	merged_stats(stat.out.collect(), params.outdir)


}




