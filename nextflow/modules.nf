#!/usr/bin/env nextflow

nextflow.enable.dsl = 2

process prepare_rawdata {
    
	errorStrategy 'retry'
    maxRetries 3
    input:
        tuple val(prefix), file(reads)
        path workdir

    output:
        tuple  val(prefix), path("${prefix}/prepare_rawdata")

    storeDir "${workdir}" 

    script:
    """
        echo $reads
        mkdir -p ${prefix}/prepare_rawdata/

        r1=`echo ${reads} | awk '{print \$1}'`
        r2=`echo ${reads} | awk '{print \$2}'`

        cp  \${r1} ${prefix}/prepare_rawdata/${prefix}_1.fastq.gz 
        cp  \${r2} ${prefix}/prepare_rawdata/${prefix}_2.fastq.gz
    
    """

}


process step1 {

	errorStrategy 'retry'
    maxRetries 3
    input:
        tuple val(prefix), path(reads_dir)
        path workdir
	
	output:
        tuple  val(prefix), path("${prefix}/step1")

	storeDir "${workdir}"

	script:
	"""
		mkdir -p ${prefix}/step1
		/opt/cpu stag -W -T 18 -t ${task.cpus} -O ${prefix}/step1/${prefix} ${reads_dir}/${prefix}_1.fastq.gz ${reads_dir}/${prefix}_2.fastq.gz
		/opt/cpu stat -s -p -T 18 -t ${task.cpus} ${prefix}/step1/${prefix}.cpu > ${prefix}/step1/${prefix}.stat
		pigz -p ${task.cpus} ${prefix}/step1/*fastq

	"""
}


process step2_1 {

	errorStrategy 'retry'
    maxRetries 3
    input:
        tuple val(prefix), path(input_dir)
		path ref_dir
		val ref_prefix
		path workdir

	output:
		tuple  val(prefix), path("${prefix}/step2_1")

	storeDir "${workdir}"

	script:
	"""
		mkdir -p ${prefix}/step2_1
		/opt/cpu memaln -T 30 -t ${task.cpus} ${ref_dir}/${ref_prefix} ${input_dir}/${prefix}.none.fastq.gz > ${prefix}/step2_1/${prefix}.none.sam
		pigz -p ${task.cpus} ${prefix}/step2_1/${prefix}.none.sam
		
		/opt/cpu pair -S -q 30 -t ${task.cpus} ${prefix}/step2_1/${prefix}.none.sam.gz > ${prefix}/step2_1/${prefix}.none.stat.xls
		/opt/cpu span -g -t ${task.cpus} ${prefix}/step2_1/${prefix}.none.UU.bam > ${prefix}/step2_1/${prefix}.UU.span.gz		

		/opt/cpu dedup -g -t ${task.cpus} ${prefix}/step2_1/${prefix}.none.UU.bam 1> ${prefix}/step2_1/${prefix}.none.UU.dedup.lc
 		/opt/cpu span -t ${task.cpus} ${prefix}/step2_1/${prefix}.none.UU.nr.bam > ${prefix}/step2_1/${prefix}.none.UU.nr.span.xls
	"""
}


process step2_2 {

	errorStrategy 'retry'
    maxRetries 3
    input:
        tuple val(prefix), path(input_dir)
        path ref_dir
        val ref_prefix
        path workdir

    output:
        tuple  val(prefix), path("${prefix}/step2_2")

    storeDir "${workdir}"

	script:
	"""
		mkdir -p ${prefix}/step2_2
		/opt/cpu memaln -T 10 -t ${task.cpus} ${ref_dir}/${ref_prefix} ${input_dir}/${prefix}.singlelinker.single.fastq.gz > ${prefix}/step2_2/${prefix}.singlelinker.single.sam
		pigz -p ${task.cpus} ${prefix}/step2_2/${prefix}.singlelinker.single.sam
		/opt/cpu pair -S -q 10 -t ${task.cpus} ${prefix}/step2_2/${prefix}.singlelinker.single.sam.gz > ${prefix}/step2_2/${prefix}.singlelinker.single.stat.xls
		/opt/cpu span -g -t ${task.cpus} ${prefix}/step2_2/${prefix}.singlelinker.single.UxxU.bam > ${prefix}/step2_2/${prefix}.singlelinker.single.UxxU.span.xls
		/opt/cpu dedup -g -t ${task.cpus} ${prefix}/step2_2/${prefix}.singlelinker.single.UxxU.bam > ${prefix}/step2_2/${prefix}.singlelinker.single.UxxU.dedup.lc
		rm ${prefix}/step2_2/${prefix}.singlelinker.single.UxxU.cpu.dedup

		/opt/cpu span -t ${task.cpus} ${prefix}/step2_2/${prefix}.singlelinker.single.UxxU.nr.bam > ${prefix}/step2_2/${prefix}.singlelinker.single.UxxU.nr.span.xls

	"""
}

process step2_3 {

	errorStrategy 'retry'
    maxRetries 1
    input:
        tuple val(prefix), path(input_dir)
        path ref_dir
        val ref_prefix
        path workdir

    output:
        tuple  val(prefix), path("${prefix}/step2_3")

    storeDir "${workdir}"

    script:
    """
        mkdir -p ${prefix}/step2_3
		/opt/cpu memaln -T 30 -t ${task.cpus} ${ref_dir}/${ref_prefix} ${input_dir}/${prefix}.singlelinker.paired.fastq.gz > ${prefix}/step2_3/${prefix}.singlelinker.paired.sam
		pigz -p ${task.cpus} ${prefix}/step2_3/${prefix}.singlelinker.paired.sam
		/opt/cpu pair -S -s 8000 -t ${task.cpus} -q 30 ${prefix}/step2_3/${prefix}.singlelinker.paired.sam.gz > ${prefix}/step2_3/${prefix}.singlelinker.paired.stat.xls
		/opt/cpu span -g -s 8000 -t ${task.cpus} ${prefix}/step2_3/${prefix}.singlelinker.paired.UU.bam > ${prefix}/step2_3/${prefix}.singlelinker.paired.UU.span.xls
		/opt/cpu dedup -g -s 8000 -t ${task.cpus} ${prefix}/step2_3/${prefix}.singlelinker.paired.UU.bam > ${prefix}/step2_3/${prefix}.singlelinker.paired.UU.dedup.lc
		/opt/cpu span -s 8000 -t ${task.cpus} ${prefix}/step2_3/${prefix}.singlelinker.paired.UU.nr.bam > ${prefix}/step2_3/${prefix}.singlelinker.paired.UU.nr.span.xls
		/opt/cpu cluster -m -s 8000 -B 1000 -5 5,0 -3 3,500 -t ${task.cpus} -j -x -v 1 -g  -O ${prefix}/step2_3/${prefix}.e500 ${prefix}/step2_3/${prefix}.singlelinker.paired.UU.nr.bam
		#rm ${prefix}/step2_3/${prefix}.e500.cpu.cluster
		#java -Xmx32g -jar /opt/juicer_tools.1.7.5_linux_x64_jcuda.0.8.jar ${prefix}/step2_3/${prefix}.e500.juice.gz ${prefix}/step2_3/${prefix}.hic ${ref_dir}/${ref_prefix}
		java -Xmx32g -jar /opt/juicer_tools.1.7.5_linux_x64_jcuda.0.8.jar pre -r 2500000,1000000,500000,250000,100000,50000,25000,10000,5000,1000  ${prefix}/step2_3/${prefix}.e500.juice.gz ${prefix}/step2_3/${prefix}.hic hg38				

		#java -Xmx32g -jar /opt/juicer_tools.1.6.2_linux_jcuda.0.8.jar ${prefix}/step2_3/${prefix}.e500.juice.gz ${prefix}/step2_3/${prefix}.hic ${ref_dir}/${ref_prefix}
		#java -Xmx32g -jar /opt/juicer_tools.1.6.2_linux_jcuda.0.8.jar pre -r 2500000,1000000,500000,250000,100000,50000,25000,10000,5000,1000  ${prefix}/step2_3/${prefix}.e500.juice.gz ${prefix}/step2_3/${prefix}.hic hg38
	
	"""
}




process call_peaks_macs {

	errorStrategy 'retry'
    maxRetries 1   
 
	input:
		tuple val(prefix1), path(none_path)
		tuple val(prefix2), path(single_path)
		tuple val(prefix3), path(paired_path)
        path workdir

	output:
		tuple val(prefix1), path("${prefix1}/call_peaks")
	
	storeDir "${workdir}"

	script:
	"""
		mkdir -p ${prefix1}/call_peaks
		samtools sort -m 300M -@ ${task.cpus} ${paired_path}/${prefix3}.singlelinker.paired.UU.nr.bam -o ${prefix1}/call_peaks/${prefix3}.singlelinker.paired.UU.nr.sorted.bam
		samtools sort -m 300M -@ ${task.cpus} ${single_path}/${prefix2}.singlelinker.single.UxxU.nr.bam -o ${prefix1}/call_peaks/${prefix3}.singlelinker.single.UU.nr.sorted.bam	
		samtools sort -m 300M -@ ${task.cpus} ${none_path}/${prefix2}.none.UU.nr.bam -o ${prefix1}/call_peaks/${prefix3}.none.UU.nr.sorted.bam

		samtools merge -f -@ ${task.cpus} ${prefix1}/call_peaks/${prefix1}.for.BROWSER.bam ${prefix1}/call_peaks/${prefix3}.singlelinker.paired.UU.nr.sorted.bam ${prefix1}/call_peaks/${prefix3}.singlelinker.single.UU.nr.sorted.bam ${prefix1}/call_peaks/${prefix3}.none.UU.nr.sorted.bam
		
		macs --keep-dup all --nomodel -t ${prefix1}/call_peaks/${prefix1}.for.BROWSER.bam -f BAM -g hs -s 500 -p 1e-10 -n ${prefix1}/call_peaks/${prefix1}.no_input_all
		
		bedtools genomecov -ibam ${prefix1}/call_peaks/${prefix1}.for.BROWSER.bam -bg > ${prefix1}/call_peaks/${prefix1}.for.BROWSER.bedgraph

	"""
}


process call_peaks_macs2 {

    errorStrategy 'retry'
    maxRetries 1

    input:
        tuple val(prefix1), path(none_path)
        tuple val(prefix2), path(single_path)
        tuple val(prefix3), path(paired_path)
        //path ref_dir
        //val ref_prefix
        path workdir

    output:
        tuple val(prefix1), path("${prefix1}/call_peaks")

    storeDir "${workdir}"

    script:
    """
        mkdir -p ${prefix1}/call_peaks
        samtools sort -m 300M -@ ${task.cpus} ${paired_path}/${prefix3}.singlelinker.paired.UU.nr.bam -o ${prefix1}/call_peaks/${prefix3}.singlelinker.paired.UU.nr.sorted.bam
        samtools sort -m 300M -@ ${task.cpus} ${single_path}/${prefix2}.singlelinker.single.UxxU.nr.bam -o ${prefix1}/call_peaks/${prefix3}.singlelinker.single.UU.nr.sorted.bam
        samtools sort -m 300M -@ ${task.cpus} ${none_path}/${prefix2}.none.UU.nr.bam -o ${prefix1}/call_peaks/${prefix3}.none.UU.nr.sorted.bam

        samtools merge -f -@ ${task.cpus} ${prefix1}/call_peaks/${prefix1}.for.BROWSER.bam ${prefix1}/call_peaks/${prefix3}.singlelinker.paired.UU.nr.sorted.bam ${prefix1}/call_peaks/${prefix3}.singlelinker.single.UU.nr.sorted.bam ${prefix1}/call_peaks/${prefix3}.none.UU.nr.sorted.bam

        #macs --keep-dup all --nomodel -t ${prefix1}/call_peaks/${prefix1}.for.BROWSER.bam -f BAM -g hs -s 500 -p 1e-10 -n ${prefix1}/call_peaks/${prefix1}.no_input_all
		macs2 callpeak --keep-dup all --nomodel -t ${prefix1}/call_peaks/${prefix1}.for.BROWSER.bam -f BAM -g hs -n ${prefix1}/call_peaks/${prefix1}.no_input_all

        bedtools genomecov -ibam ${prefix1}/call_peaks/${prefix1}.for.BROWSER.bam -bg > ${prefix1}/call_peaks/${prefix1}.for.BROWSER.bedgraph

    """
}


process stat {

	errorStrategy 'retry'
    maxRetries 1

    input:
		tuple val(prefix4), path(step1_path)
        tuple val(prefix1), path(none_path)
        tuple val(prefix2), path(single_path)
        tuple val(prefix3), path(paired_path)
		tuple val(prefix),  path(peaks_path)		
        path workdir

	output:
		tuple val(prefix1), path("${prefix1}/stat")
	
	storeDir "${workdir}"

	script:
	"""
		mkdir -p ${prefix1}/stat
		
		cp -r ${step1_path}/* ./
		cp -r ${single_path}/* ./
		cp -r ${none_path}/* ./
		cp -r ${peaks_path}/* ./	
		cp -r ${paired_path}/* ./	
	
		/opt/extract_summary_stats.sh ${prefix1}
	
		echo "perl -p -i -e  's/\\\\\\\\t/\\t/g' ${prefix1}.final_stats.tsv" | bash	
		cp ${prefix1}.final_stats.tsv ${prefix1}/stat/
		rm -rf *bam *gz

	"""
}


process merged_stats {

	errorStrategy 'retry'
    maxRetries 1

    input:
		val in_str
		path workdir
	output:
		path "merged_stats"

	storeDir "${workdir}"

	script:
	"""
		mkdir -p merged_stats/tmp
		dirs=`echo $in_str | sed 's/,//g' | sed 's/\\]//g' | awk '{for(i=1;i<=NF;i++)if(i%2==0){ print \$i}}'`

		for	dir in \$dirs
		do
			cp \$dir/*tsv merged_stats/tmp
		done

		paste merged_stats/tmp/*tsv | awk '{c=\$1;for(i=1;i<=NF;i++){if(i%2 == 0){c=c"\t"\$i}};print c}' > merged_stats/merged.tsv	
		rm -rf merged_stats/tmp
	"""	


}






